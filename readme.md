# Installation de l'application
La procédure ci-dessous détail l'installation des 7 microservices qui composent l'application :
- apigw (la gateway entre le front et le back)
- authService (service d'authentification et de gestion des token API)
- msfront (microservice de l'interface graphique de l'application)
- mspatient (microservice pour la gestion des patients)
- msnotes (microservice pour l'enregistrement des notes des différents patients)
- msassess (microservice pour l'évaluation du risque)

## Prérequis
Pour pouvoir lancer l'application, il est nécessaire d'avoir installer sur la machine :
- docker
- docker compose
- maven


## Démarrage
### Première étape : package des différents microservices
Pour réaliser le packging de tous les microservices, il faut se positionner dans le dossier source, 
qui contient l'ensemble des fichiers :

`./medilabo-solutions-oc-p9`

puis exécuter la commande :
`mvn clean package`

cela permet de nettoyer et de packager l'ensemble de fichier jar

### Seconde étape : lancement des applications
L'ensemble des microservices sont "dockerisé", il faut simplement :
- s'assurer que docker et docker compose sont correctement installés et que les services sont lancés sur la machine
- se positionner dans le dossier racine contenant l'ensemble des micro services
- exécuter la commande : `docker compose up -d`

## Utilisation
Un compte de tests est disponible à tous pour tester l'application. Pour se connecter :
- utilisateur: **test@email.com**
- mot de passe: **password**

Ensuite vous pouvez librement utiliser les fonctionnalités de l'application pour :
- créer de nouveaux patients
- lui associer des notes
- regarder la notation
