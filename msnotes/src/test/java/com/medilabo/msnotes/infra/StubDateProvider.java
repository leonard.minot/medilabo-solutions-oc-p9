package com.medilabo.msnotes.infra;

import com.medilabo.msnotes.domain.DateProvider;

import java.time.LocalDateTime;

public class StubDateProvider implements DateProvider {
    public LocalDateTime now;
    @Override
    public LocalDateTime getNow() {
        return now;
    }
}
