package com.medilabo.msnotes.infra;

import com.medilabo.msnotes.domain.Note;
import com.medilabo.msnotes.domain.NotesRepository;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
public class FakeNotesRepository implements NotesRepository {
    public List<Note> notes = new ArrayList<>();


    @Override
    public void save(Note note) {
        notes.add(note);
    }

    @Override
    public void updateNote(Note noteToUpdate) {
        notes = notes.stream()
                .filter(note -> !note.noteId().equals(noteToUpdate.noteId()))
                .collect(Collectors.toCollection(ArrayList::new));
        notes.add(noteToUpdate);
        log.info("Notes: {}", notes);
    }

    @Override
    public Optional<Note> getById(UUID id) {;
        return notes.stream().filter(note -> note.noteId().equals(id)).findAny();
    }

    @Override
    public List<Note> getNotesForPatient(UUID patientId) {
        return notes.stream().filter(note -> note.patientId().equals(patientId)).toList();
    }

    @Override
    public void deleteNote(UUID id) {
        notes = notes.stream().filter(note -> !note.noteId().equals(id)).toList();
    }
}
