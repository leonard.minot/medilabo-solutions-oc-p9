package com.medilabo.msnotes.application.usecases;

import com.medilabo.msnotes.domain.Note;
import org.junit.jupiter.api.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Tag("UnitTest")
public class CreateNotesUsecaseTest {
    private NotesFixture fixture;

    @BeforeEach
    public void setUp() {
        fixture = new NotesFixture();
    }

    @Nested
    @DisplayName("Feature: create new note")
    class CreateNewNote {
        @Test
        void itShouldSaveANewNoteInRepository() {
            // Given
            fixture.givenNowIs(LocalDateTime.of(2024, 6, 10, 9,14,0));

            // When
            fixture.whenCreateANewNote(UUID.fromString("99999999-1111-1111-1111-111111111111"), "Victor", "Contenu de la note prise par le medecin");

            // Then
            fixture.thenNotesRepositoryShouldContain(new Note(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    UUID.fromString("99999999-1111-1111-1111-111111111111"),
                    "Victor",
                    "Contenu de la note prise par le medecin",
                    LocalDateTime.of(2024, 6, 10, 9,14,0),
                    LocalDateTime.of(2024, 6, 10, 9,14,0)
            ));
        }

        @Test
        void itShouldThrowIfPatientNameIsEmpty() {
            // Given
            fixture.givenNowIs(LocalDateTime.of(2024, 6, 10, 9,14,0));

            // When...Then
            fixture.whenCreateANewNoteThenThrow(UUID.fromString("99999999-1111-1111-1111-111111111111"), "", "note");
        }

        @Test
        void itShouldThrowIfNoteIsEmpty() {
            // Given
            fixture.givenNowIs(LocalDateTime.of(2024, 6, 10, 9,14,0));

            // When...Then
            fixture.whenCreateANewNoteThenThrow(UUID.fromString("99999999-1111-1111-1111-111111111111"), "Victor", "");
        }
    }
}
