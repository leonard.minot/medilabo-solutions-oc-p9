package com.medilabo.msnotes.application.usecases;

import com.medilabo.msnotes.domain.Note;
import org.junit.jupiter.api.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Tag("UnitTest")
public class DeleteNoteUsecaseTest {
    private NotesFixture fixture;

    @BeforeEach
    void setUp() {
        fixture = new NotesFixture();
    }

    @Nested
    @DisplayName("Feature: Delete a note by it's Id")
    class DeleteNoteFeature {
        @Test
        void itShouldDeleteANoteByItsId() {
            // Given
            fixture.givenExistingNote(new Note(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    UUID.fromString("99999999-1111-1111-1111-111111111111"),
                    "Victor",
                    "Contenu de la note",
                    LocalDateTime.of(2024, 6, 10, 9, 14, 0),
                    LocalDateTime.of(2024, 6, 10, 9, 14, 0)
            ));

            fixture.givenExistingNote(new Note(
                    UUID.fromString("11111111-1111-2222-1111-111111111111"),
                    UUID.fromString("99999999-1111-1111-1111-111111111111"),
                    "Victor",
                    "Contenu de la seconde note",
                    LocalDateTime.of(2024, 6, 10, 9, 14, 0),
                    LocalDateTime.of(2024, 6, 10, 9, 14, 0)
            ));

            fixture.givenExistingNote(new Note(
                    UUID.fromString("11111111-1111-3333-1111-111111111111"),
                    UUID.fromString("88888888-1111-1111-1111-111111111111"),
                    "Leo",
                    "Note d'un autre patient",
                    LocalDateTime.of(2024, 6, 10, 9, 14, 0),
                    LocalDateTime.of(2024, 6, 10, 9, 14, 0)
            ));

            // When
            fixture.whenDeleteNote(UUID.fromString("11111111-1111-2222-1111-111111111111"));

            // Then
            fixture.thenNotesRepositoryShouldContainAll(List.of(
                    new Note(
                            UUID.fromString("11111111-1111-1111-1111-111111111111"),
                            UUID.fromString("99999999-1111-1111-1111-111111111111"),
                            "Victor",
                            "Contenu de la note",
                            LocalDateTime.of(2024, 6, 10, 9, 14, 0),
                            LocalDateTime.of(2024, 6, 10, 9, 14, 0)
                    ),
                    new Note(
                            UUID.fromString("11111111-1111-3333-1111-111111111111"),
                            UUID.fromString("88888888-1111-1111-1111-111111111111"),
                            "Leo",
                            "Note d'un autre patient",
                            LocalDateTime.of(2024, 6, 10, 9, 14, 0),
                            LocalDateTime.of(2024, 6, 10, 9, 14, 0)
                    )
            ));
        }
    }
}
