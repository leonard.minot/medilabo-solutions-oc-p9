package com.medilabo.msnotes.application.usecases;

import com.medilabo.msnotes.domain.Note;
import org.junit.jupiter.api.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Tag("UnitTest")
public class UpdateNoteUsecaseTest {
    private NotesFixture fixture;

    @BeforeEach
    public void setUp() {
        fixture = new NotesFixture();
    }

    @Nested
    @DisplayName("Feature: update a note")
    class UpdateNote {
        @Test
        void itShouldUpdateANoteByItsId() {
            // Given
            fixture.givenExistingNote(new Note(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    UUID.fromString("99999999-1111-1111-1111-111111111111"),
                    "Victor",
                    "Note initiale",
                    LocalDateTime.of(2024, 6, 10, 9,14,0),
                    LocalDateTime.of(2024, 6, 10, 9,14,0)
            ));

            fixture.givenNowIs(LocalDateTime.of(2024, 6, 10, 10,0,0));

            // When
            fixture.whenUpdateNote(UUID.fromString("11111111-1111-1111-1111-111111111111"), "Note mise à jour");

            // Then
            fixture.thenNotesRepositoryShouldContain(new Note(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    UUID.fromString("99999999-1111-1111-1111-111111111111"),
                    "Victor",
                    "Note mise à jour",
                    LocalDateTime.of(2024, 6, 10, 9,14,0),
                    LocalDateTime.of(2024, 6, 10, 10,0,0)
            ));
        }

        @Test
        void itShouldThrowWhenUpdatedUnknownNote() {
            // Given
            fixture.givenExistingNote(new Note(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    UUID.fromString("99999999-1111-1111-1111-111111111111"),
                    "Victor",
                    "Note initiale",
                    LocalDateTime.of(2024, 6, 10, 9,14,0),
                    LocalDateTime.of(2024, 6, 10, 9,14,0)
            ));

            fixture.givenNowIs(LocalDateTime.of(2024, 6, 10, 10,0,0));

            // When...Then
            fixture.whenUpdateNoteThenThrow(UUID.fromString("11111111-1111-1111-2222-111111111111"), "Note mise à jour");
        }

        @Test
        void itShouldThrowWhenUpdatedNoteWithEmptyContent() {
            // Given
            fixture.givenExistingNote(new Note(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    UUID.fromString("99999999-1111-1111-1111-111111111111"),
                    "Victor",
                    "Note initiale",
                    LocalDateTime.of(2024, 6, 10, 9,14,0),
                    LocalDateTime.of(2024, 6, 10, 9,14,0)
            ));

            fixture.givenNowIs(LocalDateTime.of(2024, 6, 10, 10,0,0));

            //When...Then
            fixture.whenUpdateNoteWithEmptyContentThenThrow(UUID.fromString("11111111-1111-1111-1111-111111111111"), "");
        }
    }
}
