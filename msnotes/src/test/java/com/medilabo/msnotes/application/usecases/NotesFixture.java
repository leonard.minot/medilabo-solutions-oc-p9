package com.medilabo.msnotes.application.usecases;

import com.medilabo.msnotes.application.dto.CreateNoteCommand;
import com.medilabo.msnotes.application.dto.UpdateNoteCommand;
import com.medilabo.msnotes.domain.Note;
import com.medilabo.msnotes.infra.FakeNotesRepository;
import com.medilabo.msnotes.infra.StubDateProvider;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class NotesFixture {

    private final StubDateProvider dateProvider = new StubDateProvider();
    private final FakeNotesRepository notesRepository = new FakeNotesRepository();
    private final CreateNoteUsecase createNoteUsecase = new CreateNoteUsecase(notesRepository);
    private final UpdateNoteUsecase updateNoteUsecase = new UpdateNoteUsecase(notesRepository);
    private final GetNotesUsecase getNotesUsecase = new GetNotesUsecase(notesRepository);
    private final DeleteNoteUsecase deleteNoteUsecase = new DeleteNoteUsecase(notesRepository);

    private Optional<Note> fetchedNote;
    private List<Note> fetchedNotes = new ArrayList<>();

    public void givenNowIs(LocalDateTime now) {
        dateProvider.now = now;
    }

    public void givenExistingNote(Note note) {
        notesRepository.notes.add(note);
    }

    public void whenCreateANewNote(UUID patientId, String patientName, String note) {
        createNoteUsecase.apply(new CreateNoteCommand(UUID.fromString("11111111-1111-1111-1111-111111111111"),patientId, patientName, note, dateProvider.getNow()));
    }

    public void whenUpdateNote(UUID noteId, String noteContent) {
        updateNoteUsecase.apply(new UpdateNoteCommand(noteId, noteContent, dateProvider.getNow()));
    }

    public void whenGetNoteByNoteId(UUID noteId) {
        fetchedNote = getNotesUsecase.getNoteById(noteId);
    }

    public void whenFetchAllNotesForAPatient(UUID patientId) {
        fetchedNotes = getNotesUsecase.getNotesForPatient(patientId);
    }

    public void whenDeleteNote(UUID noteId) {
        deleteNoteUsecase.apply(noteId);
    }

    public void thenNotesRepositoryShouldContain(Note expectedNote) {
        assertThat(notesRepository.notes).contains(expectedNote);
    }

    public void thenFetchedNoteShouldBe(Note expectedNote) {
        assertThat(fetchedNote.get()).isEqualTo(expectedNote);
    }

    public void thenFetchedPatientNotesShouldBe(List<Note> expectedNotes) {
        assertThat(fetchedNotes).isEqualTo(expectedNotes);
    }

    public void thenNotesRepositoryShouldContainAll(List<Note> expectedNotes) {
        assertThat(notesRepository.notes).hasSameElementsAs(expectedNotes);
    }

    public void whenCreateANewNoteThenThrow(UUID patientId, String patientName, String note) {
        assertThatThrownBy(() -> createNoteUsecase.apply(new CreateNoteCommand(UUID.fromString("11111111-1111-1111-1111-111111111111"), patientId, patientName, note, dateProvider.getNow())))
                .hasMessageContaining("All fields (patientId, patientName, note) are required")
                .isInstanceOf(IllegalArgumentException.class);
    }

    public void whenUpdateNoteThenThrow(UUID noteId, String noteContent) {
        assertThatThrownBy(() -> updateNoteUsecase.apply(new UpdateNoteCommand(noteId, noteContent, dateProvider.getNow())))
                .hasMessageContaining("Unknown note id: " + noteId.toString())
                .isInstanceOf(IllegalArgumentException.class);
    }

    public void whenUpdateNoteWithEmptyContentThenThrow(UUID noteId, String noteContent) {
        assertThatThrownBy(() -> updateNoteUsecase.apply(new UpdateNoteCommand(noteId, noteContent, dateProvider.getNow())))
                .hasMessageContaining("Note content is empty")
                .isInstanceOf(IllegalArgumentException.class);
    }

    public void whenGetNoteByNoteIdThenThrow(UUID noteId) {
        assertThatThrownBy(() -> getNotesUsecase.getNoteById(noteId))
                .hasMessageContaining("Note not found")
                .isInstanceOf(IllegalStateException.class);
    }
}
