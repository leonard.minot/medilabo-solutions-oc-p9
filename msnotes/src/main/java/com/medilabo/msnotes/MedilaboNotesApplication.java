package com.medilabo.msnotes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MedilaboNotesApplication {
    public static void main(String[] args) {
        SpringApplication.run(MedilaboNotesApplication.class, args);
    }
}
