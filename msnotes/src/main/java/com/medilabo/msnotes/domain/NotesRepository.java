package com.medilabo.msnotes.domain;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface NotesRepository {
    void save(Note note);
    void updateNote(Note note);
    Optional<Note> getById(UUID id);
    List<Note> getNotesForPatient(UUID patientId);
    void deleteNote(UUID id);
}
