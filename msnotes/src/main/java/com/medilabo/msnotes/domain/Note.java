package com.medilabo.msnotes.domain;

import java.time.LocalDateTime;
import java.util.UUID;

public record Note(
        UUID noteId,
        UUID patientId,
        String patientName,
        String note,
        LocalDateTime createdAt,
        LocalDateTime updatedAt
) {
}
