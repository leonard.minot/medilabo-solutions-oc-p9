package com.medilabo.msnotes.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface DateProvider {
    LocalDateTime getNow();
}
