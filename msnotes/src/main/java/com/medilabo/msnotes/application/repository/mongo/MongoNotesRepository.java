package com.medilabo.msnotes.application.repository.mongo;

import com.medilabo.msnotes.application.entity.NoteEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MongoNotesRepository extends MongoRepository<NoteEntity, String> {
}
