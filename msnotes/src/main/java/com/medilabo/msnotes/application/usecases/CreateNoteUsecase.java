package com.medilabo.msnotes.application.usecases;

import com.medilabo.msnotes.application.dto.CreateNoteCommand;
import com.medilabo.msnotes.domain.Note;
import com.medilabo.msnotes.domain.NotesRepository;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class CreateNoteUsecase {

    private final NotesRepository notesRepository;

    public CreateNoteUsecase(NotesRepository notesRepository) {
        this.notesRepository = notesRepository;
    }

    public void apply(CreateNoteCommand noteCommand) {
        if (noteCommand.patientName().isEmpty() || noteCommand.note().isEmpty())
            throw new IllegalArgumentException("All fields (patientId, patientName, note) are required");

        notesRepository.save(new Note(
                noteCommand.noteId(),
                noteCommand.patientId(),
                noteCommand.patientName(),
                noteCommand.note(),
                noteCommand.noteDate(),
                noteCommand.noteDate()
        ));
    }
}
