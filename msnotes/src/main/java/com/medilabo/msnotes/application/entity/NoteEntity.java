package com.medilabo.msnotes.application.entity;

import com.medilabo.msnotes.domain.Note;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@Data
@NoArgsConstructor
@Document(value = "notes")
public class NoteEntity {
    @Id
    private String noteId;

    private String patientId;

    private String patientName;

    private String note;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    public static NoteEntity from(Note note) {
        return new NoteEntity(
                note.noteId().toString(),
                note.patientId().toString(),
                note.patientName(),
                note.note(),
                note.createdAt(),
                note.updatedAt()
        );
    }

    public Note toDomain() {
        return new Note(
                UUID.fromString(this.noteId),
                UUID.fromString(this.patientId),
                this.patientName,
                this.note,
                this.createdAt,
                this.updatedAt
        );
    }
}
