package com.medilabo.msnotes.application.usecases;

import com.medilabo.msnotes.application.dto.UpdateNoteCommand;
import com.medilabo.msnotes.domain.Note;
import com.medilabo.msnotes.domain.NotesRepository;
import org.springframework.stereotype.Component;

@Component
public class UpdateNoteUsecase {
    private final NotesRepository notesRepository;

    public UpdateNoteUsecase(NotesRepository notesRepository) {
        this.notesRepository = notesRepository;
    }

    public void apply(UpdateNoteCommand updateNoteCommand) {
        Note currentNote = notesRepository
                .getById(updateNoteCommand.noteId())
                .orElseThrow(() ->
                        new IllegalArgumentException("Unknown note id: " + updateNoteCommand.noteId().toString()));

        if (updateNoteCommand.noteContent().isEmpty())
            throw new IllegalArgumentException("Note content is empty");

        notesRepository.updateNote(new Note(
                updateNoteCommand.noteId(),
                currentNote.patientId(),
                currentNote.patientName(),
                updateNoteCommand.noteContent(),
                currentNote.createdAt(),
                updateNoteCommand.updatedAt()
        ));
    }
}
