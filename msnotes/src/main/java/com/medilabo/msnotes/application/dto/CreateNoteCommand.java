package com.medilabo.msnotes.application.dto;

import java.time.LocalDateTime;
import java.util.UUID;

public record CreateNoteCommand(
        UUID noteId,
        UUID patientId,
        String patientName,
        String note,
        LocalDateTime noteDate
) {
}
