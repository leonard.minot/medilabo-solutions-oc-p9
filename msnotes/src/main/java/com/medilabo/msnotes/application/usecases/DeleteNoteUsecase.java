package com.medilabo.msnotes.application.usecases;

import com.medilabo.msnotes.domain.NotesRepository;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class DeleteNoteUsecase {
    private final NotesRepository notesRepository;

    public DeleteNoteUsecase(NotesRepository notesRepository) {
        this.notesRepository = notesRepository;
    }

    public void apply(UUID noteId) {
        notesRepository.deleteNote(noteId);
    }
}
