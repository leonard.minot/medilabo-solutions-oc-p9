package com.medilabo.msnotes.application.repository;


import com.medilabo.msnotes.application.entity.NoteEntity;
import com.medilabo.msnotes.application.repository.mongo.MongoNotesRepository;
import com.medilabo.msnotes.domain.Note;
import com.medilabo.msnotes.domain.NotesRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class NotesRepositoryImpl implements NotesRepository {

    private final MongoNotesRepository mongoNotesRepository;

    public NotesRepositoryImpl(MongoNotesRepository mongoNotesRepository) {
        this.mongoNotesRepository = mongoNotesRepository;
    }

    @Override
    public void save(Note note) {
        mongoNotesRepository.save(NoteEntity.from(note));
    }

    @Override
    public void updateNote(Note note) {
        mongoNotesRepository.save(NoteEntity.from(note));
    }

    @Override
    public Optional<Note> getById(UUID id) {
        return mongoNotesRepository.findById(id.toString()).map(NoteEntity::toDomain);
    }

    @Override
    public List<Note> getNotesForPatient(UUID patientId) {
        return mongoNotesRepository
                .findAll().stream()
                .filter(noteEntity -> noteEntity.getPatientId().equals(patientId.toString()))
                .map(NoteEntity::toDomain)
                .toList();
    }

    @Override
    public void deleteNote(UUID id) {
        mongoNotesRepository.deleteById(id.toString());
    }
}
