package com.medilabo.msnotes.application.controller;

import com.medilabo.msnotes.application.dto.CreateNoteCommand;
import com.medilabo.msnotes.application.dto.UpdateNoteCommand;
import com.medilabo.msnotes.application.entity.NoteEntity;
import com.medilabo.msnotes.application.usecases.CreateNoteUsecase;
import com.medilabo.msnotes.application.usecases.DeleteNoteUsecase;
import com.medilabo.msnotes.application.usecases.GetNotesUsecase;
import com.medilabo.msnotes.application.usecases.UpdateNoteUsecase;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
public class NotesController {
    private final CreateNoteUsecase createNoteUsecase;
    private final UpdateNoteUsecase updateNoteUsecase;
    private final DeleteNoteUsecase deleteNoteUsecase;
    private final GetNotesUsecase getNotesUsecase;

    public NotesController(CreateNoteUsecase createNoteUsecase, UpdateNoteUsecase updateNoteUsecase, DeleteNoteUsecase deleteNoteUsecase, GetNotesUsecase getNotesUsecase) {
        this.createNoteUsecase = createNoteUsecase;
        this.updateNoteUsecase = updateNoteUsecase;
        this.deleteNoteUsecase = deleteNoteUsecase;
        this.getNotesUsecase = getNotesUsecase;
    }

    @PostMapping("api/v1/note")
    public void createNote(@RequestBody NoteEntity note) {
        createNoteUsecase.apply(new CreateNoteCommand(
                UUID.randomUUID(),
                UUID.fromString(note.getPatientId()),
                note.getPatientName(),
                note.getNote(),
                LocalDateTime.now()
        ));
    }

    @PutMapping("api/v1/note")
    public void updateNote(@RequestBody NoteEntity note) {
        updateNoteUsecase.apply(new UpdateNoteCommand(
                UUID.fromString(note.getNoteId()),
                note.getNote(),
                LocalDateTime.now()
        ));
    }

    @DeleteMapping("api/v1/note/{id}")
    public void deleteNote(@PathVariable("id") String id) {
        deleteNoteUsecase.apply(UUID.fromString(id));
    }

    @GetMapping("api/v1/notes/{patientId}")
    public List<NoteEntity> getNoteForPatients(@PathVariable("patientId") String patientId) {
        return getNotesUsecase.getNotesForPatient(UUID.fromString(patientId)).stream().map(NoteEntity::from).toList();
    }

    // TODO: gérer le retour en cas de message vide
    @GetMapping("api/v1/note/{noteId}")
    public NoteEntity getNote(@PathVariable("noteId") String noteId) {
        return NoteEntity.from(getNotesUsecase.getNoteById(UUID.fromString(noteId)).get());
    }
}
