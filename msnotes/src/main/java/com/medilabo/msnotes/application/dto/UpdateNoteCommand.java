package com.medilabo.msnotes.application.dto;

import java.time.LocalDateTime;
import java.util.UUID;

public record UpdateNoteCommand(
        UUID noteId,
        String noteContent,
        LocalDateTime updatedAt
) {
}
