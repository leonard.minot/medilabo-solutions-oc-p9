package com.medilabo.msnotes.application.usecases;

import com.medilabo.msnotes.domain.Note;
import com.medilabo.msnotes.domain.NotesRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class GetNotesUsecase {

    private final NotesRepository notesRepository;

    public GetNotesUsecase(NotesRepository notesRepository) {
        this.notesRepository = notesRepository;
    }

    public Optional<Note> getNoteById(UUID id) {
        Optional<Note> noteFound = notesRepository.getById(id);
        if (noteFound.isEmpty())
            throw new IllegalStateException("Note not found");

        return noteFound;
    }

    public List<Note> getNotesForPatient(UUID patientId) {
        return notesRepository.getNotesForPatient(patientId);
    }
}
