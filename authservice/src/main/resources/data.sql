INSERT INTO user_credential (id, email, name, password, role)
VALUES ('6cdfc0b2-3af5-4694-a152-889f6286948d',
        'test@email.com',
        'Compte test',
        '$2y$10$PL1k6heIivASgzBZ7blmEOLOzHIgHrsCqr9SSwEBNFidyswTiYlKS',
        'DOCTOR')
ON CONFLICT DO NOTHING;
