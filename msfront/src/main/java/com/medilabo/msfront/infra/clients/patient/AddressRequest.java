package com.medilabo.msfront.infra.clients.patient;

public record AddressRequest(
        String number,
        String streetName
) {
}
