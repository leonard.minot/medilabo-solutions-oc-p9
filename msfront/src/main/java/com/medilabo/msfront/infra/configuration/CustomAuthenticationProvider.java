package com.medilabo.msfront.infra.configuration;

import com.medilabo.msfront.infra.clients.auth.AuthRequest;
import com.medilabo.msfront.infra.clients.auth.AuthServiceClient;
import com.medilabo.msfront.infra.entity.UserCredential;
import com.medilabo.msfront.infra.repository.UserAccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private final UserAccountRepository userAccountRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final AuthServiceClient authServiceClient;

    public CustomAuthenticationProvider(UserAccountRepository userAccountRepository, BCryptPasswordEncoder passwordEncoder, AuthServiceClient authServiceClient) {
        this.userAccountRepository = userAccountRepository;
        this.passwordEncoder = passwordEncoder;
        this.authServiceClient = authServiceClient;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        if(validUser(username, password)) {
            String jwt = authServiceClient.getJwtToken(new AuthRequest(username, password));
            List<GrantedAuthority> authorities = new ArrayList<>();
            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(username, null, authorities);
            authToken.setDetails(jwt);
            return authToken;
        } else {
            throw new BadCredentialsException("Invalid username or password");
        }

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

    private boolean validUser(String username, String password) {
        UserCredential user = userAccountRepository.findByEmailEquals(username).orElseThrow(() -> new UsernameNotFoundException("User not found"));
        if (user != null && passwordEncoder.matches(password, user.getPassword())) {
            return true;
        }
        return false;
    }
}
