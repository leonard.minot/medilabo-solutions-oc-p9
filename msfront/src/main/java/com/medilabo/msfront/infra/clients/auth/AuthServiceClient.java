package com.medilabo.msfront.infra.clients.auth;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(url = "http://authservice:8120", name = "authentication")
public interface AuthServiceClient {
    @PostMapping("/auth/token")
    String getJwtToken(@RequestBody AuthRequest authRequest);
}
