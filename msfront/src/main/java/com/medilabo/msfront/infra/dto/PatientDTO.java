package com.medilabo.msfront.infra.dto;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class PatientDTO {
    public UUID id;
    public String lastName;
    public String firstName;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public LocalDate dateOfBirth;
    public String gender;
    public String addressNumber;
    public String addressStreet;
    public String phoneNumber;
    public String doctor;
}
