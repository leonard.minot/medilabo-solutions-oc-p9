package com.medilabo.msfront.infra.clients.auth;

public record AuthRequest(String name, String password) {
}
