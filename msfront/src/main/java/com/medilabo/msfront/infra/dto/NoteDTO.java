package com.medilabo.msfront.infra.dto;

import lombok.*;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class NoteDTO {
    public UUID patientId;
    public String note;
    public String patientName;
}
