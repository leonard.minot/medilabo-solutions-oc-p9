package com.medilabo.msfront.infra.clients.patient;

import java.time.LocalDate;
import java.util.UUID;

public record PatientsResponse(
        UUID id,
        String firstName,
        String lastName,
        LocalDate dateOfBirth,
        String gender,
        AddressResponse address,
        String phoneNumber,
        String doctor
) {
}
