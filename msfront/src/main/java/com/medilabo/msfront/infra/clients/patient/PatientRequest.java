package com.medilabo.msfront.infra.clients.patient;

import java.time.LocalDate;

public record PatientRequest(
        String lastName,
        String firstName,
        LocalDate dateOfBirth,
        String gender,
        AddressRequest address,
        String phoneNumber
) {
}
