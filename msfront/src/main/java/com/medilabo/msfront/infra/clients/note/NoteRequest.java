package com.medilabo.msfront.infra.clients.note;

import java.time.LocalDateTime;
import java.util.UUID;

public record NoteRequest(
        UUID noteId,
        UUID patientId,
        String patientName,
        String note,
        LocalDateTime createdAt
) {
}
