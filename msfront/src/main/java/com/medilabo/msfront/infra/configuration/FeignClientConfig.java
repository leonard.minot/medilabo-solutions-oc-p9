package com.medilabo.msfront.infra.configuration;

import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;

import java.util.logging.Logger;

public class FeignClientConfig {
    @Bean
    public System.Logger.Level feignLoggerLevel() {
        return System.Logger.Level.ALL;
    }

    @Bean
    public RequestInterceptor requestInterceptor() {
        return new FeignClientAuthInterceptor();
    }
}
