package com.medilabo.msfront.infra.configuration;

import com.medilabo.msfront.infra.clients.auth.AuthServiceClient;
import com.medilabo.msfront.infra.repository.UserAccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@Slf4j
public class AuthConfig {

    private final CustomUserDetailsService customUserDetailsService;
    private final AuthServiceClient authServiceClient;
    private final UserAccountRepository userAccountRepository;

    public AuthConfig(CustomUserDetailsService customUserDetailsService, AuthServiceClient authServiceClient, UserAccountRepository userAccountRepository) {
        this.customUserDetailsService = customUserDetailsService;
        this.authServiceClient = authServiceClient;
        this.userAccountRepository = userAccountRepository;
        log.info("AuthConfig loaded");
    }


    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .authorizeHttpRequests(auth -> auth.anyRequest().authenticated())
                .formLogin(form -> form
                        .permitAll()
//                        .defaultSuccessUrl("/")
                        .successHandler(new CustomAuthenticationSuccessHandler())
                )
                .authenticationProvider(new CustomAuthenticationProvider(userAccountRepository, passwordEncoder(), authServiceClient))
                .build();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authenticationManager(HttpSecurity http, BCryptPasswordEncoder bCryptPasswordEncoder) throws Exception {
        AuthenticationManagerBuilder authenticationManagerBuilder = http
                .getSharedObject(AuthenticationManagerBuilder.class);
        authenticationManagerBuilder.userDetailsService(customUserDetailsService).passwordEncoder(bCryptPasswordEncoder);
        return authenticationManagerBuilder.build();
    }

}

