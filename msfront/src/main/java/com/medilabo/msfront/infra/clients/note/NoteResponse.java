package com.medilabo.msfront.infra.clients.note;

import java.time.LocalDateTime;
import java.util.UUID;

public record NoteResponse(
        UUID noteId,
        UUID patientId,
        String patientName,
        String note,
        LocalDateTime createdAt,
        LocalDateTime updatedAt
) {
}
