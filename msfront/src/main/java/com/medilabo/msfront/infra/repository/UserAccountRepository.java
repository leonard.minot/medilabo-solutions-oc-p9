package com.medilabo.msfront.infra.repository;

import com.medilabo.msfront.infra.entity.UserCredential;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserAccountRepository extends JpaRepository<UserCredential, UUID> {
    Optional<UserCredential> findByEmailEquals(String email);
}
