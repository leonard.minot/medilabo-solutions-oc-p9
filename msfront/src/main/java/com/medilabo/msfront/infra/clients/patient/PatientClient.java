package com.medilabo.msfront.infra.clients.patient;

import com.medilabo.msfront.infra.configuration.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@FeignClient(url = "http://apigw:8100", name = "patient", configuration = FeignClientConfig.class)
public interface PatientClient {
    @GetMapping("api/v1/patients")
    List<PatientsResponse> getPatients();

    @PostMapping("api/v1/patient")
    void createPatient(PatientRequest patientRequest);

    @DeleteMapping("api/v1/patient/{id}")
    void deletePatient(@PathVariable("id") UUID id);

    @GetMapping("api/v1/patient/{id}")
    PatientsResponse getPatientById(@PathVariable("id") UUID id);

    @PutMapping("api/v1/patient")
    void updatePatient(PatientRequestWithId patient);
}
