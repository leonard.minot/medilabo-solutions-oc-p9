package com.medilabo.msfront.infra.clients.assessment;

import com.medilabo.msfront.infra.configuration.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

@FeignClient(url = "http://apigw:8100", name = "assessment", configuration = FeignClientConfig.class)
public interface AssessmentClient {
    @GetMapping("api/v1/assessment/{id}")
    PatientAssessment getAssessmentForPatient(@PathVariable("id") UUID id);
}
