package com.medilabo.msfront.infra.clients.assessment;

public enum PatientAssessment {
    NONE, BORDERLINE, IN_DANGER, EARLY_ONSET
}
