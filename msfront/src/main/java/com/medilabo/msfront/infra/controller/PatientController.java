package com.medilabo.msfront.infra.controller;

import com.medilabo.msfront.infra.clients.assessment.AssessmentClient;
import com.medilabo.msfront.infra.clients.assessment.PatientAssessment;
import com.medilabo.msfront.infra.clients.note.NoteClient;
import com.medilabo.msfront.infra.clients.note.NoteRequest;
import com.medilabo.msfront.infra.clients.note.NoteResponse;
import com.medilabo.msfront.infra.clients.patient.*;
import com.medilabo.msfront.infra.dto.NoteDTO;
import com.medilabo.msfront.infra.dto.PatientDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Controller
@Slf4j
@AllArgsConstructor
public class PatientController {

    private final PatientClient patientClient;
    private final NoteClient noteClient;
    private final AssessmentClient assessmentClient;

    @GetMapping("/patients")
    public String patientsList(Model model) {
        List<PatientsResponse> patients = patientClient.getPatients();
        log.info("Patients list: {}", patients);
        model.addAttribute("patients", patients);
        return "patients-list";
    }

    @GetMapping("/createPatient")
    public String createPatient(Model model) {
        model.addAttribute("patient", new PatientDTO());
        return "create-patient";
    }

    @PostMapping("/createPatient")
    public String postPatient(@ModelAttribute("patient") PatientDTO patientDTO) {
        log.info("Created patient: {}", patientDTO);
        PatientRequest newPatient = new PatientRequest(
                patientDTO.lastName,
                patientDTO.firstName,
                patientDTO.dateOfBirth,
                patientDTO.gender,
                new AddressRequest(
                        patientDTO.addressNumber,
                        patientDTO.addressStreet
                ),
                patientDTO.phoneNumber
        );
        patientClient.createPatient(newPatient);
        return "redirect:/patients";
    }

    // TODO: lors de la suppression d'un patient, il faut également supprimer toutes les notes associées
    @PostMapping("/deletePatient")
    public String deletePatient(@RequestParam UUID patientId) {
        log.info("Patient deleted processing");
        patientClient.deletePatient(patientId);
        return "redirect:/patients";
    }

    @GetMapping("/updatePatient/{id}")
    public String updatePatient(@PathVariable("id") UUID id, Model model, Principal principal) {
        PatientsResponse patient = patientClient.getPatientById(id);
        PatientDTO patientToUpdate = new PatientDTO(
                patient.id(),
                patient.lastName(),
                patient.firstName(),
                patient.dateOfBirth(),
                patient.gender(),
                patient.address().number(),
                patient.address().streetName(),
                patient.phoneNumber(),
                principal.getName()
        );
        model.addAttribute("patient", patientToUpdate);
        return "update-patient";
    }

    @PostMapping("/updatePatient/{id}")
    public String postUpdatePatient(@ModelAttribute("patient") PatientDTO patientDTO, Principal principal) {
        log.info("Updated patient: {}", patientDTO);
        patientClient.updatePatient(new PatientRequestWithId(
                patientDTO.id,
                patientDTO.lastName,
                patientDTO.firstName,
                patientDTO.dateOfBirth,
                patientDTO.gender,
                new AddressRequest(
                        patientDTO.addressNumber,
                        patientDTO.addressStreet
                ),
                patientDTO.phoneNumber,
                principal.getName()

        ));
        return "redirect:/patient/" + patientDTO.id;
    }

    @GetMapping("/")
    public String welcomePage() {
        return "welcome";
    }

    @GetMapping("/patient/{id}")
    public String patientInfo(@PathVariable("id") UUID patientID, Model model) {
        PatientsResponse patient = patientClient.getPatientById(patientID);
        List<NoteResponse> notes = noteClient.getNotesFor(patientID).stream()
                .sorted(Comparator.comparing(NoteResponse::createdAt).reversed())
                .toList();
        PatientAssessment assessment = assessmentClient.getAssessmentForPatient(patientID);
        log.info("Patient assessment: {}", assessment);

        model.addAttribute("patient", patient);
        model.addAttribute("notes", notes);
        model.addAttribute("newNoteContent", new NoteDTO(patient.id(), "", patient.firstName() + " " + patient.lastName()));
        model.addAttribute("assessment", assessment);

        return "patient-info";
    }

    @PostMapping("/postNote")
    public String postNewNote(@ModelAttribute("newNoteContent") NoteDTO newNoteContent) {
        NoteRequest noteRequest = new NoteRequest(
                UUID.randomUUID(),
                newNoteContent.patientId,
                newNoteContent.patientName,
                newNoteContent.note,
                LocalDateTime.now()
        );
        noteClient.postNote(noteRequest);
        return "redirect:/patient/" + newNoteContent.getPatientId();
    }

    @PostMapping("/deleteNote")
    public String deleteNote(@RequestParam UUID noteId) {
        NoteResponse noteInfo = noteClient.getNote(noteId);
        noteClient.deleteNote(noteId);
        return "redirect:/patient/" + noteInfo.patientId();
    }

    @PostMapping("/updateNote")
    public String updateNote(@RequestParam UUID noteId, @RequestParam String noteContent) {
        NoteResponse noteInfo = noteClient.getNote(noteId);
        NoteRequest noteRequest = new NoteRequest(
                noteId,
                noteInfo.patientId(),
                noteInfo.patientName(),
                noteContent,
                LocalDateTime.now()
        );
        noteClient.updateNote(noteRequest);

        return "redirect:/patient/" + noteInfo.patientId();
    }
}
