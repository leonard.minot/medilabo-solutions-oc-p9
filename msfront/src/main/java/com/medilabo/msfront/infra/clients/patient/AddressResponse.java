package com.medilabo.msfront.infra.clients.patient;

import java.util.UUID;

public record AddressResponse(
        UUID id,
        String number,
        String streetName
) {
}
