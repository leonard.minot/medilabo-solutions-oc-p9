package com.medilabo.msfront.infra.clients.patient;

import java.time.LocalDate;
import java.util.UUID;

public record PatientRequestWithId(
        UUID id,
        String lastName,
        String firstName,
        LocalDate dateOfBirth,
        String gender,
        AddressRequest address,
        String phoneNumber,
        String doctor
) {
}
