package com.medilabo.msfront.infra.clients.note;

import com.medilabo.msfront.infra.configuration.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@FeignClient(url = "http://apigw:8100", name = "note", configuration = FeignClientConfig.class)
public interface NoteClient {
    @GetMapping("api/v1/notes/{id}")
    List<NoteResponse> getNotesFor(@PathVariable("id")UUID patientID);

    @PostMapping("api/v1/note")
    void postNote(NoteRequest noteRequest);

    @GetMapping("api/v1/note/{id}")
    NoteResponse getNote(@PathVariable("id") UUID id);

    @DeleteMapping("api/v1/note/{id}")
    void deleteNote(@PathVariable("id") UUID id);

    @PutMapping("api/v1/note")
    void updateNote(NoteRequest noteRequest);
}
