package com.medilabo.mspatient.domain;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AddressRepository {
    Address createAddress(Address address);
    List<Address> getAll();
    Optional<Address> getById(UUID id);
    Optional<Address> getByAddress(String number, String street);
}
