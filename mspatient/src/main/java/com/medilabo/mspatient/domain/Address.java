package com.medilabo.mspatient.domain;

import java.util.Objects;
import java.util.UUID;

public record Address(
        UUID id,
        String number,
        String streetName
) {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(number, address.number) && Objects.equals(streetName, address.streetName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, streetName);
    }
}
