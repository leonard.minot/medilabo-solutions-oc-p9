package com.medilabo.mspatient.infra.usecase;

import com.medilabo.mspatient.domain.Patient;
import com.medilabo.mspatient.domain.PatientRepository;
import com.medilabo.mspatient.infra.exceptions.PatientNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Component
public class GetPatientsUsecase {
    private final PatientRepository patientRepository;

    public GetPatientsUsecase(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<Patient> getAllPatients(String doctor) {
        return patientRepository.getAll().stream().filter(patient -> patient.doctor().equals(doctor)).toList();
    }

    public Patient getPatientById(UUID id) {
        Optional<Patient> maybePatient = patientRepository.getById(id);
        return maybePatient.orElseThrow(() -> new PatientNotFoundException("Patient not found."));
    }
}
