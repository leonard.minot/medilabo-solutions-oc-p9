package com.medilabo.mspatient.infra.usecase;

import com.medilabo.mspatient.domain.Patient;
import com.medilabo.mspatient.domain.PatientRepository;
import com.medilabo.mspatient.infra.exceptions.PatientNotFoundException;
import org.springframework.stereotype.Component;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

@Component
public class DeletePatientUsecase {

    private final PatientRepository patientRepository;

    public DeletePatientUsecase(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public void deletePatientUsecase(UUID id) {
        Optional<Patient> maybePatient = patientRepository.getById(id);
        maybePatient.ifPresentOrElse(
                patient -> patientRepository.delete(patient.id()),
                () -> { throw new PatientNotFoundException("Patient not found - Impossible to delete."); }
        );
    }
}
