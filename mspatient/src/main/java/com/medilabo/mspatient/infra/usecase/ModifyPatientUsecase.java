package com.medilabo.mspatient.infra.usecase;

import com.medilabo.mspatient.domain.Address;
import com.medilabo.mspatient.domain.AddressService;
import com.medilabo.mspatient.domain.Patient;
import com.medilabo.mspatient.domain.PatientRepository;
import com.medilabo.mspatient.infra.exceptions.PatientNotFoundException;
import org.springframework.stereotype.Component;

import java.util.NoSuchElementException;
import java.util.Optional;

@Component
public class ModifyPatientUsecase {
    private final PatientRepository patientRepository;
    private final AddressService addressService;

    public ModifyPatientUsecase(PatientRepository patientRepository, AddressService addressService) {
        this.patientRepository = patientRepository;
        this.addressService = addressService;
    }

    public void modifyPatientUsecase(Patient patientToUpdate) {
        Optional<Patient> currentPatient = patientRepository.getById(patientToUpdate.id());
        currentPatient.ifPresentOrElse(
                patient -> {
                    Address patientAddress = addressService.getOrCreateAddress(patientToUpdate.address());
                    patientRepository.update(new Patient(
                            patientToUpdate.id(),
                            patientToUpdate.lastName(),
                            patientToUpdate.firstName(),
                            patientToUpdate.dateOfBirth(),
                            patientToUpdate.gender(),
                            patientAddress,
                            patientToUpdate.phoneNumber(),
                            patientToUpdate.doctor()
                    ));
                },
                () -> {
                    throw new PatientNotFoundException("Patient not found - Impossible to update.");
                }
        );

    }
}
