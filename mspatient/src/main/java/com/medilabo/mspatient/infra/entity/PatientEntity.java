package com.medilabo.mspatient.infra.entity;

import com.medilabo.mspatient.domain.Patient;
import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table( name = "patient")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class PatientEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id")
    private UUID id;

    @Column(
            name = "last_name",
            nullable = false
    )
    private String lastName;

    @Column(
            name = "first_name",
            nullable = false
    )
    private String firstName;

    @Column(
            name = "date_of_birth",
            nullable = false
    )
    private LocalDate dateOfBirth;

    @Column(
            name = "gender",
            nullable = false
    )
    private String gender;

    @Column(
            name = "phone_number",
            nullable = false
    )
    private String phoneNumber;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(
            name = "address_id",
            referencedColumnName = "id"
    )
    private AddressEntity address;

    @Column(
            name = "doctor",
            nullable = false
    )
    private String doctor;

    public static PatientEntity from(Patient patient) {
        return new PatientEntity(
                patient.id(),
                patient.lastName(),
                patient.firstName(),
                patient.dateOfBirth(),
                patient.gender(),
                AddressEntity.from(patient.address()),
                patient.phoneNumber(),
                patient.doctor()
        );
    }

    public Patient toDomain() {
        return new Patient(
                this.id,
                this.lastName,
                this.firstName,
                this.dateOfBirth,
                this.gender,
                this.address.toDomain(),
                this.phoneNumber,
                this.doctor
        );
    }

    public PatientEntity(UUID id, String lastName, String firstName, LocalDate dateOfBirth, String gender, AddressEntity address, String phoneNumber, String doctor) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.doctor = doctor;
    }
}
