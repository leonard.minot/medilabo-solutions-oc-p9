package usecases;

import com.medilabo.mspatient.domain.Address;
import com.medilabo.mspatient.domain.Patient;
import com.medilabo.mspatient.infra.dto.CreatePatientCommand;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.util.UUID;

@Tag("UnitTest")
public class CreatePatientUsecaseTest {
    private PatientFixture fixture;

    @BeforeEach
    void setUp() {
        fixture = new PatientFixture();
    }

    @Nested
    @DisplayName("Feature: Add Patient to repository")
    class AddPatient {
        @Test
        void itShouldCreateANewPatientAndANewAddress() {
            // Given
            fixture.givenNextPatientUUIDToBe("1124d9e8-6266-4bcf-8035-37a02ba75c69");
            fixture.givenNextAddressUUIDToBe("11111111-2222-3333-4444-555555555555");

            // When
            fixture.whenCreateNewPatient(new CreatePatientCommand(
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.randomUUID(), "1", "Brookside St"),
                    "100-222-3333",
                    "Jean-Jacques"
            ));

            // Then
            fixture.thenPatientRepositoryShouldContain(new Patient(
                    UUID.fromString("1124d9e8-6266-4bcf-8035-37a02ba75c69"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("11111111-2222-3333-4444-555555555555"), "1", "Brookside St"),
                    "100-222-3333",
                    "Jean-Jacques"
            ));

            fixture.thenAddressRepositoryShouldContain(new Address(
                    UUID.fromString("11111111-2222-3333-4444-555555555555"),
                    "1",
                    "Brookside St"
            ));
        }

        @Test
        void itShouldCreateANewPatientLivingInAlreadyExistingAddress() {
            // Given
            fixture.givenNextPatientUUIDToBe("11111111-1111-1111-1111-111111111111");
            fixture.givenExistingAddress(new Address(
                    UUID.fromString("22222222-1111-1111-1111-111111111111"),
                    "7",
                    "Route du fief"
            ));

            // When
            fixture.whenCreateNewPatient(new CreatePatientCommand(
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.randomUUID(), "7", "Route du fief"),
                    "100-222-3333",
                    "Jean-Jacques"
            ));

            // Then
            fixture.thenAddressRepositoryShouldContain(new Address(
                    UUID.fromString("22222222-1111-1111-1111-111111111111"),
                    "7",
                    "Route du fief"
            ));

            fixture.thenAddressRepositoryShouldHaveLengthOf(1);

            fixture.thenPatientRepositoryShouldContain(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("22222222-1111-1111-1111-111111111111"), "7", "Route du fief"),
                    "100-222-3333",
                    "Jean-Jacques"
            ));


        }
    }
}
