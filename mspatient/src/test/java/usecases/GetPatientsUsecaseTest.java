package usecases;

import com.medilabo.mspatient.domain.Address;
import com.medilabo.mspatient.domain.Patient;
import com.medilabo.mspatient.infra.exceptions.PatientNotFoundException;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Tag("UnitTest")
public class GetPatientsUsecaseTest {
    private PatientFixture fixture;

    @BeforeEach
    void setUp() {
        fixture = new PatientFixture();
    }

    @Nested
    @DisplayName("Feature: Get all patients")
    class GetPatients {
        @Test
        void itShouldReturnAllPatients() {
            // Given
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333",
                    "Jean-Jacques"
            ));
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("33333333-3333-3333-3333-333333333333"),
                    "Minot",
                    "Léonard",
                    LocalDate.of(1991, 8, 16),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333",
                    "Jean-Jacques"
            ));
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("33333333-3333-3333-3333-333333333333"),
                    "Nick",
                    "Suzuki",
                    LocalDate.of(1999, 8, 10),
                    "M",
                    new Address(UUID.fromString("22222222-4444-2222-2222-222222222222"), "10", "Route Joliette"),
                    "444-444-4444",
                    "Pierre"
            ));

            // When
            fixture.whenFetchingAllPatients("Jean-Jacques");

            // Then
            fixture.thenPatientRepositoryShouldContainAll(List.of(
                    new Patient(
                            UUID.fromString("11111111-1111-1111-1111-111111111111"),
                            "Minot",
                            "Victor",
                            LocalDate.of(2020, 5, 17),
                            "M",
                            new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                            "100-222-3333",
                            "Jean-Jacques"
                    ),
                    new Patient(
                            UUID.fromString("33333333-3333-3333-3333-333333333333"),
                            "Minot",
                            "Léonard",
                            LocalDate.of(1991, 8, 16),
                            "M",
                            new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                            "100-222-3333",
                            "Jean-Jacques"
                    )
            ));
        }

        @Test
        void itShouldReturnAPatientByItsId() {
            // Given
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333",
                    "Jean-Jacques"
            ));
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("33333333-3333-3333-3333-333333333333"),
                    "Minot",
                    "Léonard",
                    LocalDate.of(1991, 8, 16),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333",
                    "Jean-Jacques"
            ));

            // When
            fixture.whenFetchingPatientByItsId(UUID.fromString("11111111-1111-1111-1111-111111111111"));

            // Then
            fixture.thenPatientShouldBe(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333",
                    "Jean-Jacques"
            ));
        }

        @Test
        void itShouldThrowIfPatientIdIsUnknown() {
            // Given
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333",
                    "Jean-Jacques"
            ));

            // When...Then
            fixture.whenGetUnknownPatientThenThrow(UUID.fromString("22222222-1111-1111-1111-111111111111"), new PatientNotFoundException("Patient not found."));
        }
    }
}
