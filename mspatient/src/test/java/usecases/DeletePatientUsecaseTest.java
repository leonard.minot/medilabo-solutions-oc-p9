package usecases;

import com.medilabo.mspatient.domain.Address;
import com.medilabo.mspatient.domain.Patient;
import com.medilabo.mspatient.infra.exceptions.PatientNotFoundException;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.util.NoSuchElementException;
import java.util.UUID;

@Tag("UnitTest")
public class DeletePatientUsecaseTest {
    private PatientFixture fixture;

    @BeforeEach
    void setUp() {
        fixture = new PatientFixture();
    }

    @Nested
    @DisplayName("Feature: delete an existing patient")
    class DeleteExistingPatient {
        @Test
        void itShouldDeleteAnExistingPatient() {
            // Given
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333",
                    "Jean-Jacques"
            ));
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("33333333-3333-3333-3333-333333333333"),
                    "Minot",
                    "Léonard",
                    LocalDate.of(1991, 8, 16),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333",
                    "Jean-Jacques"
            ));

            // When
            fixture.whenDeleteAPatientById(UUID.fromString("33333333-3333-3333-3333-333333333333"));

            // Then
            fixture.thenPatientRepositoryShouldContain(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333",
                    "Jean-Jacques"
            ));
            fixture.thenPatientRepositoryShouldHaveLengthOf(1);
        }

        @Test
        void itShouldThrowIfUnknownPatient() {
            // Given
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("33333333-3333-3333-3333-333333333333"),
                    "Minot",
                    "Léonard",
                    LocalDate.of(1991, 8, 16),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333",
                    "Jean-Jacques"
            ));

            // When
            // Then
            fixture.whenDeleteANotKnownPatientThenThrow(UUID.fromString("11111111-1111-1111-1111-111111111111"), new PatientNotFoundException("Patient not found - Impossible to delete."));

        }
    }
}
