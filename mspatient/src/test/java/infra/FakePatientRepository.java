package infra;

import com.medilabo.mspatient.domain.Patient;
import com.medilabo.mspatient.domain.PatientRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class FakePatientRepository implements PatientRepository {
    public UUID nextUUID;
    public List<Patient> patients = new ArrayList<>();

    /**
     * Patient UUID is overridden in the test to the next UUID given by the test fixture
     */
    @Override
    public void savePatient(Patient patient) {
        patients.add(new Patient(
                nextUUID,
                patient.lastName(),
                patient.firstName(),
                patient.dateOfBirth(),
                patient.gender(),
                patient.address(),
                patient.phoneNumber(),
                patient.doctor()
        ));
    }

    @Override
    public List<Patient> getAll() {
        return patients;
    }

    @Override
    public Optional<Patient> getById(UUID id) {
        return patients.stream().filter(patient -> patient.id().equals(id)).findAny();
    }

    @Override
    public void update(Patient patient) {
        patients = patients.stream()
                .map(pat -> (pat.id().equals(patient.id())) ? patient : pat)
                .toList();
    }

    @Override
    public void delete(UUID id) {
        patients = patients.stream()
                .filter(patient -> !patient.id().equals(id))
                .toList();
    }
}
