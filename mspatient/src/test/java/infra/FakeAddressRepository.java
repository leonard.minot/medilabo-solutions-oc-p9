package infra;

import com.medilabo.mspatient.domain.Address;
import com.medilabo.mspatient.domain.AddressRepository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

public class FakeAddressRepository implements AddressRepository {

    public UUID nextUUID;
    public ArrayList<Address> addresses = new ArrayList<>();

    @Override
    public Address createAddress(Address address) {
        Address newAddress = new Address(
                nextUUID,
                address.number(),
                address.streetName()
        );
        addresses.add(newAddress);
        return newAddress;
    }

    @Override
    public ArrayList<Address> getAll() {
        return addresses;
    }

    @Override
    public Optional<Address> getById(UUID id) {
        return addresses.stream().filter(address -> address.id().equals(id)).findAny();
    }

    @Override
    public Optional<Address> getByAddress(String number, String street) {
        return addresses.stream()
                .filter(address -> address.number().equals(number) && address.streetName().equals(street))
                .findAny();
    }
}
