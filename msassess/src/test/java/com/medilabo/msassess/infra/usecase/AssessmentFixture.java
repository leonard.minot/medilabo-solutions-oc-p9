package com.medilabo.msassess.infra.usecase;

import com.medilabo.msassess.domain.Patient;
import com.medilabo.msassess.domain.PatientAssessment;
import com.medilabo.msassess.infra.FakeNotesGateway;
import com.medilabo.msassess.infra.FakePatientGateway;
import com.medilabo.msassess.infra.StubDateProvider;
import com.medilabo.msassess.infra.exceptions.NotesGatewayException;
import com.medilabo.msassess.infra.exceptions.PatientGatewayException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AssessmentFixture {
    private final StubDateProvider stubDateProvider = new StubDateProvider();
    private final FakePatientGateway  fakePatientGateway = new FakePatientGateway();
    private final FakeNotesGateway fakeNotesGateway = new FakeNotesGateway();
    private final GetPatientAssessmentUsecase usecase = new GetPatientAssessmentUsecase(fakePatientGateway, fakeNotesGateway, stubDateProvider);
    public List<String> notes = new ArrayList<>();
    private PatientAssessment assessmentResult;

    public void givenNowIs(LocalDate now) {
        stubDateProvider.now = now;
    }

    public void givenNotes(List<String> notes) {
        this.notes.addAll(notes);
    }

    public void givenExistingRemoteNotes(List<String> notes) {
        fakeNotesGateway.notesResponse = notes;
    }

    public void givenExistingRemotePatient(Patient patient) {
        fakePatientGateway.patientResponse = patient;
    }

    public Map<String, Integer> whenCountTriggerWithAssessmentMap(Map<String, Integer> assessmentMap) {
        return usecase.getTriggerCount(notes, assessmentMap);
    }

    public void whenAssessPatient(UUID patientId, Map<String, Integer> assessmentMap) {
        assessmentResult = usecase.assessPatient(patientId, assessmentMap);
    }

    public void thenAssessmentShouldBe(PatientAssessment expectedAssessment) {
        assertThat(assessmentResult).isEqualTo(expectedAssessment);
    }

    public void whenAssessPatientThenThrow(UUID patientId, Map<String, Integer> assessmentMap) {
        assertThatThrownBy(() -> assessmentResult = usecase.assessPatient(patientId, assessmentMap))
                .isInstanceOf(PatientGatewayException.class)
                .hasMessageContaining("Error with patient Gateway while looking for patient: 11111111-1111-1111-1111-111111111111");
    }
    public void whenAssessPatientWithNoNotesThenThrow(UUID patientId, Map<String, Integer> assessmentMap) {
        assertThatThrownBy(() -> assessmentResult = usecase.assessPatient(patientId, assessmentMap))
                .isInstanceOf(NotesGatewayException.class)
                .hasMessageContaining("Error with notes Gateway while looking for patient: 11111111-1111-1111-1111-111111111111");
    }



}
