package com.medilabo.msassess.infra;

import com.medilabo.msassess.domain.NotesGateway;

import java.util.List;
import java.util.UUID;

public class FakeNotesGateway implements NotesGateway {
    public List<String> notesResponse;
    @Override
    public List<String> getAllNotesForPatient(UUID patientID) {
        return notesResponse;
    }
}
