package com.medilabo.msassess.infra.usecase;

import com.medilabo.msassess.domain.Address;
import com.medilabo.msassess.domain.Patient;
import com.medilabo.msassess.domain.PatientAssessment;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("UnitTest")
public class GetPatientAssessmentUsecaseTest {
    private AssessmentFixture fixture;

    @BeforeEach
    void setUp() {
        fixture = new AssessmentFixture();
    }

    @Nested
    @DisplayName("Feature: count the number of triggers from notes")
    class CountTriggers {
        @Test
        void itShouldCountWordsWithNoCaseSensitive() {
            // Given
            fixture.givenNotes(List.of("Détection de microalbumine", "Deuxième note", "présence de cholestérol"));

            // ... assessment map
            Map<String, Integer> assessmentMap = getVoidAssessmentMap();

            // When
            Map<String, Integer> triggerCount = fixture.whenCountTriggerWithAssessmentMap(assessmentMap);

            // Then
            Map<String, Integer> expectedTriggerCount = new HashMap<>();
            expectedTriggerCount.put("HEMOGLOBINE A1C", 0);
            expectedTriggerCount.put("MICROALBUMINE", 1);
            expectedTriggerCount.put("TAILLE", 0);
            expectedTriggerCount.put("POIDS", 0);
            expectedTriggerCount.put("FUMEUR", 0);
            expectedTriggerCount.put("FUMEUSE", 0);
            expectedTriggerCount.put("ANORMAL", 0);
            expectedTriggerCount.put("CHOLESTEROL", 1);
            expectedTriggerCount.put("VERTIGES", 0);
            expectedTriggerCount.put("RECHUTE", 0);
            expectedTriggerCount.put("REACTION", 0);
            expectedTriggerCount.put("ANTICORPS", 0);


            assertThat(triggerCount).isEqualTo(expectedTriggerCount);
        }

        @Test
        void itShouldCountMultipleWords() {
            // Given
            fixture.givenNotes(List.of("Détection de microalbumine", "Apparition de vertiges", "toujours des vertiges", "poids trop élevé"));

            // ... assessment map
            Map<String, Integer> assessmentMap = getVoidAssessmentMap();

            // When
            Map<String, Integer> triggerCount = fixture.whenCountTriggerWithAssessmentMap(assessmentMap);

            // Then
            Map<String, Integer> expectedTriggerCount = new HashMap<>();
            expectedTriggerCount.put("HEMOGLOBINE A1C", 0);
            expectedTriggerCount.put("MICROALBUMINE", 1);
            expectedTriggerCount.put("TAILLE", 0);
            expectedTriggerCount.put("POIDS", 1);
            expectedTriggerCount.put("FUMEUR", 0);
            expectedTriggerCount.put("FUMEUSE", 0);
            expectedTriggerCount.put("ANORMAL", 0);
            expectedTriggerCount.put("CHOLESTEROL", 0);
            expectedTriggerCount.put("VERTIGES", 2);
            expectedTriggerCount.put("RECHUTE", 0);
            expectedTriggerCount.put("REACTION", 0);
            expectedTriggerCount.put("ANTICORPS", 0);


            assertThat(triggerCount).isEqualTo(expectedTriggerCount);
        }

        @Test
        void itShouldWordHemoglobineA1C() {
            // Given
            fixture.givenNotes(List.of("Détection de hémoglobine A1C", "Apparition de vertiges"));

            // ... assessment map
            Map<String, Integer> assessmentMap = getVoidAssessmentMap();

            // When
            Map<String, Integer> triggerCount = fixture.whenCountTriggerWithAssessmentMap(assessmentMap);

            // Then
            Map<String, Integer> expectedTriggerCount = new HashMap<>();
            expectedTriggerCount.put("HEMOGLOBINE A1C", 1);
            expectedTriggerCount.put("MICROALBUMINE", 0);
            expectedTriggerCount.put("TAILLE", 0);
            expectedTriggerCount.put("POIDS", 0);
            expectedTriggerCount.put("FUMEUR", 0);
            expectedTriggerCount.put("FUMEUSE", 0);
            expectedTriggerCount.put("ANORMAL", 0);
            expectedTriggerCount.put("CHOLESTEROL", 0);
            expectedTriggerCount.put("VERTIGES", 1);
            expectedTriggerCount.put("RECHUTE", 0);
            expectedTriggerCount.put("REACTION", 0);
            expectedTriggerCount.put("ANTICORPS", 0);

            assertThat(triggerCount).isEqualTo(expectedTriggerCount);
        }
    }

    private Map<String, Integer> getVoidAssessmentMap() {
        Map<String, Integer> assessmentMap = new HashMap<>();
        assessmentMap.put("HEMOGLOBINE A1C", 0);
        assessmentMap.put("MICROALBUMINE", 0);
        assessmentMap.put("TAILLE", 0);
        assessmentMap.put("POIDS", 0);
        assessmentMap.put("FUMEUR", 0);
        assessmentMap.put("FUMEUSE", 0);
        assessmentMap.put("ANORMAL", 0);
        assessmentMap.put("CHOLESTEROL", 0);
        assessmentMap.put("VERTIGES", 0);
        assessmentMap.put("RECHUTE", 0);
        assessmentMap.put("REACTION", 0);
        assessmentMap.put("ANTICORPS", 0);
        return assessmentMap;
    }

    @Nested
    @DisplayName("Feature: assess a man patient")
    class AssessMen {
        @Test
        void itShouldReturnNoneWhenManPatientAbove30With1Trigger() {
            // Given
            fixture.givenExistingRemoteNotes(List.of("Patient fumeur", "En bonne santée"));

            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leo",
                    LocalDate.of(1991, 8, 16), // Above 30
                    "M",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When
            fixture.whenAssessPatient(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());

            // Then
            fixture.thenAssessmentShouldBe(PatientAssessment.NONE);
        }

        @Test
        void itShouldReturnNoneWhenManPatientUnder30With2Triggers() {
            // Given
            fixture.givenExistingRemoteNotes(List.of("Patient fumeur", "Légère rechute"));

            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leo",
                    LocalDate.of(2000, 8, 16), // Above 30
                    "M",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When
            fixture.whenAssessPatient(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());

            // Then
            fixture.thenAssessmentShouldBe(PatientAssessment.NONE);
        }

        @Test
        void itShouldReturnInDangerWhenManPatientUnder30With3Triggers() {
            fixture.givenExistingRemoteNotes(List.of("Patient fumeur avec des vertiges", "Détection de microalbumine"));

            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leo",
                    LocalDate.of(2000, 8, 16),
                    "M",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When
            fixture.whenAssessPatient(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());

            // Then
            fixture.thenAssessmentShouldBe(PatientAssessment.IN_DANGER);
        }

        @Test
        void itShouldReturnEarlyOnSetWhenManPatientUnder30With5Triggers() {
            fixture.givenExistingRemoteNotes(List.of("Patient fumeur avec des vertiges", "Détection de microalbumine", "Poids anormal"));

            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leo",
                    LocalDate.of(2000, 8, 16), // Under 30
                    "M",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When
            fixture.whenAssessPatient(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());

            // Then
            fixture.thenAssessmentShouldBe(PatientAssessment.EARLY_ONSET);
        }

        @Test
        void itShouldReturnEarlyOnSetWhenManPatientAbove30With8Triggers() {
            fixture.givenExistingRemoteNotes(List.of("Patient fumeur avec des vertiges", "Détection de microalbumine, cholesterol et hémoglobine A1C", "Poids et taille anormal"));

            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leo",
                    LocalDate.of(1991, 8, 16), // Above 30
                    "M",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When
            fixture.whenAssessPatient(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());

            // Then
            fixture.thenAssessmentShouldBe(PatientAssessment.EARLY_ONSET);
        }

        @Test
        void itShouldReturnInDangerWhenManPatientAbove30With6Triggers() {
            fixture.givenExistingRemoteNotes(List.of("Patient avec des vertiges", "Détection de microalbumine et cholesterol", "Poids et taille anormal"));

            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leo",
                    LocalDate.of(1991, 8, 16), // Above 30
                    "M",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When
            fixture.whenAssessPatient(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());

            // Then
            fixture.thenAssessmentShouldBe(PatientAssessment.IN_DANGER);
        }

        @Test
        void itShouldReturnBorderlineWhenManPatientAbove30With2Triggers() {
            fixture.givenExistingRemoteNotes(List.of("Patient fumeur", "problème de rechute"));

            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leo",
                    LocalDate.of(1991, 8, 16), // Above 30
                    "M",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When
            fixture.whenAssessPatient(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());

            // Then
            fixture.thenAssessmentShouldBe(PatientAssessment.BORDERLINE);
        }
    }

    @Nested
    @DisplayName("Feature: assess a woman patient")
    class AssessWomen {
        @Test
        void itShouldReturnNoneWhenWomanPatientUnder30With3Triggers() {
            fixture.givenExistingRemoteNotes(List.of("Patient fumeur", "taille anormal"));

            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leia",
                    LocalDate.of(2000, 8, 16), // Under 30
                    "F",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When
            fixture.whenAssessPatient(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());

            // Then
            fixture.thenAssessmentShouldBe(PatientAssessment.NONE);
        }

        @Test
        void itShouldReturnNoneWhenWomanPatientAbove30With1Trigger() {
            fixture.givenExistingRemoteNotes(List.of("Patient fumeur", "ras"));

            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leia",
                    LocalDate.of(1991, 8, 16), // Under 30
                    "F",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When
            fixture.whenAssessPatient(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());

            // Then
            fixture.thenAssessmentShouldBe(PatientAssessment.NONE);
        }

        @Test
        void itShouldReturnEarlyOnSetWhenWomanPatientUnder30With7Triggers() {
            fixture.givenExistingRemoteNotes(List.of("Patient fumeuse, faisant des réaction aux anticorps", "taille et poids anormal", "haut niveau de cholestérol"));

            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leia",
                    LocalDate.of(2000, 8, 16), // Under 30
                    "F",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When
            fixture.whenAssessPatient(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());

            // Then
            fixture.thenAssessmentShouldBe(PatientAssessment.EARLY_ONSET);
        }

        @Test
        void itShouldReturnInDangerWhenWomanPatientUnder30With4Triggers() {
            fixture.givenExistingRemoteNotes(List.of("Patient fumeuse, faisant des réaction aux anticorps", "haut niveau de cholestérol"));

            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leia",
                    LocalDate.of(2000, 8, 16), // Under 30
                    "F",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When
            fixture.whenAssessPatient(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());

            // Then
            fixture.thenAssessmentShouldBe(PatientAssessment.IN_DANGER);
        }

        @Test
        void itShouldReturnEarlyOnSetWhenWomanPatientAbove30With8Triggers() {
            fixture.givenExistingRemoteNotes(List.of("Patient fumeuse faisant des vertiges, faisant des réaction aux anticorps", "haut niveau de cholestérol", "taille et poids anormal"));

            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leia",
                    LocalDate.of(1991, 8, 16), // Under 30
                    "F",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When
            fixture.whenAssessPatient(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());

            // Then
            fixture.thenAssessmentShouldBe(PatientAssessment.EARLY_ONSET);
        }

        @Test
        void itShouldReturnInDangerWhenWomanPatientAbove30With6Triggers() {
            fixture.givenExistingRemoteNotes(List.of("Patient fumeuse faisant des vertiges", "haut niveau de cholestérol", "taille et poids anormal"));

            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leia",
                    LocalDate.of(1991, 8, 16), // Under 30
                    "F",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When
            fixture.whenAssessPatient(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());

            // Then
            fixture.thenAssessmentShouldBe(PatientAssessment.IN_DANGER);
        }

        @Test
        void itShouldReturnBorderlineWhenWomanPatientAbove30With2Triggers() {
            fixture.givenExistingRemoteNotes(List.of("Patient fumeuse faisant des vertiges"));

            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leia",
                    LocalDate.of(1991, 8, 16), // Under 30
                    "F",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When
            fixture.whenAssessPatient(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());

            // Then
            fixture.thenAssessmentShouldBe(PatientAssessment.BORDERLINE);
        }
    }

    @Nested
    @DisplayName("Feature: Throw when error on gateway")
    class ThrowWhenErrorsOnGateway {
        @Test
        void itShouldThrowWhenUnknownPatientOnTheGateway() {
            // When ... Then
            fixture.whenAssessPatientThenThrow(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());
        }

        @Test
        void itShouldThrowWhenErrorOnNotesGateway() {
            fixture.givenNowIs(LocalDate.of(2024,7, 1));

            fixture.givenExistingRemotePatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Leia",
                    LocalDate.of(1991, 8, 16), // Under 30
                    "F",
                    new Address(
                            UUID.fromString("11111111-2222-1111-1111-111111111111"),
                            "1",
                            "Rue de la bas"
                    ),
                    "0123456987"
            ));

            // When ... Then
            fixture.whenAssessPatientWithNoNotesThenThrow(UUID.fromString("11111111-1111-1111-1111-111111111111"), getVoidAssessmentMap());
        }
    }
}
