package com.medilabo.msassess.infra;

import com.medilabo.msassess.domain.Patient;
import com.medilabo.msassess.domain.PatientGateway;

import java.util.UUID;

public class FakePatientGateway implements PatientGateway {
    public Patient patientResponse;
    @Override
    public Patient getPatientInfo(UUID patientId) {
        return patientResponse;
    }
}
