package com.medilabo.msassess.infra;

import com.medilabo.msassess.infra.provider.DateProvider;

import java.time.LocalDate;

public class StubDateProvider implements DateProvider {
    public LocalDate now;
    @Override
    public LocalDate getNow() {
        return now;
    }
}
