package com.medilabo.msassess.infra.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_GATEWAY)
public class PatientGatewayException extends RuntimeException {
    public PatientGatewayException(String message) {
        super(message);
    }
}
