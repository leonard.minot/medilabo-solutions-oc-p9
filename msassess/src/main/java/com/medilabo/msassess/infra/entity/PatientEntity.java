package com.medilabo.msassess.infra.entity;

import com.medilabo.msassess.domain.Patient;

import java.time.LocalDate;
import java.util.UUID;

public record PatientEntity(
        UUID id,
        String firstName,
        String lastName,
        LocalDate dateOfBirth,
        String gender,
        AddressEntity address,
        String phoneNumber,
        String doctor
) {
    public Patient toDomain() {
        return new Patient(
                id,
                firstName,
                lastName,
                dateOfBirth,
                gender,
                address.toDomain(),
                phoneNumber
        );
    }
}
