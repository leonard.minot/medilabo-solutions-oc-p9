package com.medilabo.msassess.infra.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(PatientGatewayException.class)
    public ResponseEntity<Object> handlePatientGatewayException(PatientGatewayException exception) {
        ApiException apiException = new ApiException(exception.getMessage(), exception, HttpStatus.BAD_GATEWAY, ZonedDateTime.now());
        return new ResponseEntity<>(apiException, apiException.getHttpStatus());
    }

    @ExceptionHandler(NotesGatewayException.class)
    public ResponseEntity<Object> handleNotesGatewayException(NotesGatewayException exception) {
        ApiException apiException = new ApiException(exception.getMessage(), exception, HttpStatus.BAD_GATEWAY, ZonedDateTime.now());
        return new ResponseEntity<>(apiException, apiException.getHttpStatus());
    }
}

