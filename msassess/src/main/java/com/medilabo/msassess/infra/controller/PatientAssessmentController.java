package com.medilabo.msassess.infra.controller;

import com.medilabo.msassess.domain.PatientAssessment;
import com.medilabo.msassess.infra.usecase.GetPatientAssessmentUsecase;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
public class PatientAssessmentController {
    private final GetPatientAssessmentUsecase getPatientAssessmentUsecase;

    public PatientAssessmentController(GetPatientAssessmentUsecase getPatientAssessmentUsecase) {
        this.getPatientAssessmentUsecase = getPatientAssessmentUsecase;
    }

    @GetMapping("api/v1/assessment/{id}")
    public PatientAssessment getAssessment(@PathVariable("id") UUID patientID) {
        Map<String, Integer> assessmentMap = new HashMap<>();
        assessmentMap.put("HEMOGLOBINE A1C", 0);
        assessmentMap.put("MICROALBUMINE", 0);
        assessmentMap.put("TAILLE", 0);
        assessmentMap.put("POIDS", 0);
        assessmentMap.put("FUMEUR", 0);
        assessmentMap.put("FUMEUSE", 0);
        assessmentMap.put("ANORMAL", 0);
        assessmentMap.put("CHOLESTEROL", 0);
        assessmentMap.put("VERTIGES", 0);
        assessmentMap.put("RECHUTE", 0);
        assessmentMap.put("REACTION", 0);
        assessmentMap.put("ANTICORPS", 0);
        return getPatientAssessmentUsecase.assessPatient(patientID, assessmentMap);
    }
}
