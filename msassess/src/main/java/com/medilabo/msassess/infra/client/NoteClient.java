package com.medilabo.msassess.infra.client;

import com.medilabo.msassess.infra.entity.NoteEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.UUID;

@FeignClient(url = "http://msnotes:8130", name = "notes")
public interface NoteClient {
    @GetMapping("api/v1/notes/{patientId}")
    List<NoteEntity> getNoteForPatient(@PathVariable("patientId") UUID patientId);
}
