package com.medilabo.msassess.infra.gateway;

import com.medilabo.msassess.domain.Patient;
import com.medilabo.msassess.domain.PatientGateway;
import com.medilabo.msassess.infra.client.PatientClient;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class PatientGatewayImpl implements PatientGateway {
    private final PatientClient patientClient;

    public PatientGatewayImpl(PatientClient patientClient) {
        this.patientClient = patientClient;
    }

    @Override
    public Patient getPatientInfo(UUID patientId) {
        return patientClient.getPatientById(patientId).toDomain();
    }
}
