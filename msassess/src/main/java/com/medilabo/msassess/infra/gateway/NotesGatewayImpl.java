package com.medilabo.msassess.infra.gateway;

import com.medilabo.msassess.domain.NotesGateway;
import com.medilabo.msassess.infra.client.NoteClient;
import com.medilabo.msassess.infra.entity.NoteEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class NotesGatewayImpl implements NotesGateway {

    private final NoteClient noteClient;

    public NotesGatewayImpl(NoteClient noteClient) {
        this.noteClient = noteClient;
    }

    @Override
    public List<String> getAllNotesForPatient(UUID patientID) {
        return noteClient.getNoteForPatient(patientID).stream().map(NoteEntity::note).toList();
    }
}
