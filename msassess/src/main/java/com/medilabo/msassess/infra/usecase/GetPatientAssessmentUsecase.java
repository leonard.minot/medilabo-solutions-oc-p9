package com.medilabo.msassess.infra.usecase;

import com.medilabo.msassess.domain.NotesGateway;
import com.medilabo.msassess.domain.Patient;
import com.medilabo.msassess.domain.PatientAssessment;
import com.medilabo.msassess.domain.PatientGateway;
import com.medilabo.msassess.infra.exceptions.NotesGatewayException;
import com.medilabo.msassess.infra.exceptions.PatientGatewayException;
import com.medilabo.msassess.infra.provider.DateProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.*;

@Slf4j
@Service
public class GetPatientAssessmentUsecase {

    private final PatientGateway patientGateway;
    private final NotesGateway notesGateway;
    private final DateProvider dateProvider;

    public GetPatientAssessmentUsecase(PatientGateway patientGateway, NotesGateway notesGateway, DateProvider dateProvider) {
        this.patientGateway = patientGateway;
        this.notesGateway = notesGateway;
        this.dateProvider = dateProvider;
    }

    public PatientAssessment assessPatient(UUID patientId, Map<String, Integer> assessmentMap) {
        Patient patient;
        try {
            patient = patientGateway.getPatientInfo(patientId);
            if (patient == null) {throw new PatientGatewayException("Error with patient Gateway while looking for patient: " + patientId);}
        } catch (Exception e) {
            throw new PatientGatewayException("Error with patient Gateway while looking for patient: " + patientId);
        }

        List<String> notes;

        try {
            notes = notesGateway.getAllNotesForPatient(patientId);
            if (notes == null) throw new NotesGatewayException("Error with notes Gateway while looking for patient: " + patientId);
        } catch (Exception e) {
            throw new NotesGatewayException("Error with notes Gateway while looking for patient: " + patientId);
        }


        LocalDate now = dateProvider.getNow();

        int patientAge = Period.between(patient.dateOfBirth(), now).getYears();
        int triggerCount = getTriggerCount(notes, assessmentMap).values().stream().mapToInt(i -> i).sum();
        String patientGender = patient.gender();

        log.info("Trigger count: {}", triggerCount);

        if (patientAge < 30) {
            if (patientGender.equals("M")) {
                if (triggerCount >= 5) {
                    return PatientAssessment.EARLY_ONSET;
                }
                if (triggerCount >= 3) {
                    return PatientAssessment.IN_DANGER;
                }
            }
            if (patientGender.equals("F")) {
                if (triggerCount >= 7) {
                    return PatientAssessment.EARLY_ONSET;
                }
                if (triggerCount >= 4) {
                    return PatientAssessment.IN_DANGER;
                }
            }
        }

        if (patientAge >= 30) {
            if (triggerCount >=8) {
                return PatientAssessment.EARLY_ONSET;
            }
            if (triggerCount >= 6) {
                return PatientAssessment.IN_DANGER;
            }
            if (triggerCount >= 2) {
                return PatientAssessment.BORDERLINE;
            }
        }
        return PatientAssessment.NONE;
    }

    public Map<String, Integer> getTriggerCount(List<String> notes, Map<String, Integer> assessmentMap) {
        notes.forEach(note -> {
                    String[] words = note.split("[\\s.,;!?]+");
                    Arrays.stream(words).forEach(word -> {
                        if (assessmentMap.containsKey(getFormattedWord(word))) {
                            assessmentMap.compute(getFormattedWord(word), (k, count) -> count + 1);
                        }
                    });
                });

        notes.forEach(note -> {
            if (getFormattedWord(note).contains("HEMOGLOBINE A1C")) {
                assessmentMap.compute("HEMOGLOBINE A1C", (k, count) -> count + 1);
            }
        });
        return assessmentMap;
    }

    private String getFormattedWord(String initialWord) {
        return initialWord
                .toUpperCase()
                .replace("È","E")
                .replace("É", "E");
    }
}
