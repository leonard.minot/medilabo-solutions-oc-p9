package com.medilabo.msassess.infra.entity;

import com.medilabo.msassess.domain.Address;

import java.util.UUID;

public record AddressEntity(
        UUID id,
        String number,
        String streetName
) {
    public Address toDomain() {
        return new Address(id, number, streetName);
    }
}
