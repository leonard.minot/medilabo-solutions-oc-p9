package com.medilabo.msassess.infra.provider;

import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class DateProviderImpl implements DateProvider {
    @Override
    public LocalDate getNow() {
        return LocalDate.now();
    }
}
