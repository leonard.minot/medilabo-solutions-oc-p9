package com.medilabo.msassess.infra.provider;

import java.time.LocalDate;

public interface DateProvider {
    LocalDate getNow();
}
