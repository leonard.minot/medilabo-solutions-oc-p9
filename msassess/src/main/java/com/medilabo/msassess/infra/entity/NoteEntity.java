package com.medilabo.msassess.infra.entity;

import java.time.LocalDateTime;

public record NoteEntity(
        String NoteId,
        String patientId,
        String patientName,
        String note,
        LocalDateTime createdAt,
        LocalDateTime updatedAt
) {
}
