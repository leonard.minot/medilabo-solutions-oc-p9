package com.medilabo.msassess.infra.client;


import com.medilabo.msassess.infra.entity.PatientEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

@FeignClient(url = "http://mspatients:8090", name = "patient")
public interface PatientClient {
    @GetMapping("api/v1/patient/{id}")
    PatientEntity getPatientById(@PathVariable("id") UUID id);
}
