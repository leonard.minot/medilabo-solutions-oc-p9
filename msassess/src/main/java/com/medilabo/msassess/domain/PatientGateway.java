package com.medilabo.msassess.domain;

import java.util.UUID;

public interface PatientGateway {
    Patient getPatientInfo(UUID patientId);
}
