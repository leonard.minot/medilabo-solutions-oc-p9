package com.medilabo.msassess.domain;

import java.util.UUID;

public record Address(
        UUID id,
        String number,
        String streetName) {
}
