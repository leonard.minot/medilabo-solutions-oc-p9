package com.medilabo.msassess.domain;

import java.util.List;
import java.util.UUID;

public interface NotesGateway {
    List<String> getAllNotesForPatient(UUID patientID);
}
