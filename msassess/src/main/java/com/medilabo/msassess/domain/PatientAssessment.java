package com.medilabo.msassess.domain;

public enum PatientAssessment {
    NONE, BORDERLINE, IN_DANGER, EARLY_ONSET
}
